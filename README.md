# Laptop Search Engine
[![pipeline status](https://gitlab.com/ferzos/laptop-search-engine/badges/master/pipeline.svg)](https://gitlab.com/ferzos/laptop-search-engine/commits/master)
[![coverage report](https://gitlab.com/ferzos/laptop-search-engine/badges/master/coverage.svg)](https://gitlab.com/ferzos/laptop-search-engine/commits/master)

Laptop Search Engines are an application that you can use to search your laptop according to your specifications. This search engine uses data from ENTERKomputer Indonesia so it can be ascertained that search results have or ever existed in Indonesia. These search engines are equipped with filtering features based on processor, RAM, capacity, brand, and price.

### Prerequisites

```
Basic knowledge of:
- REST API
- React
- Redux
- CSS Preprocessor
```

### Installing

```
1. Make sure your node version is 9.3.0
   if you using nvm: 
   1.1. nvm install 9.3.0
   1.2. nvm use 9.3.0
2. npm install
3. npm run start
```

## Built With

* [React](https://reactjs.org/) - Frontend framework
* [Axios](https://github.com/axios/axios) - Promise based HTTP client
* [Sass](https://sass-lang.com/) - CSS Preprocessor
* [Redux](https://redux.js.org/) - State container
* [React Semantic](https://react.semantic-ui.com/introduction) - CSS Library


## Authors

* **Ferdinand Chandra** - [ferzos](https://github.com/ferzos)